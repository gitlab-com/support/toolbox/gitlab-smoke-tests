# C++ Hello World

This is a "Hello World" library (``hello``) and application (``greet``)
used for demo purposes on examples about Conan C/C++ Package Manager.

## License

[MIT](LICENSE)

## Demo conan registry on GitLab

Demo video: https://www.youtube.com/watch?v=7NYgJWg-w5w

[Conan repository documentation](https://docs.gitlab.com/ee/user/packages/conan_repository/).

[Building a package documentation](https://docs.gitlab.com/ee/user/packages/workflows/build_packages.html#conan)

1. Clone this project.
1. Install [conan](https://conan.io/) version 1.60 and [cmake](https://cmake.org/install/).
1. Create [PAT](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
1. Add remote `conan remote add gitlab https://gitlab.com/api/v4/projects/47972514/packages/conan`
1. Authenticate to the remote `conan user kategrechishkina -r gitlab -p $GITLAB_API_PRIVATE_TOKEN`
1. Create conan recipe: `conan new Hello/0.1 -t`
1. Create a package for the recipe: `conan create . gitlab-gold+conan-demo/beta`
1. Upload package: `conan upload Hello/0.1@gitlab-gold+conan-demo/beta --all -r gitlab`
1. Check the package in UI.